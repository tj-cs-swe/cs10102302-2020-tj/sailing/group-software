import * as React from 'react';
import { Layout, Row, Col, Breadcrumb, Input, message, Select } from 'antd';
import { FullTeamInfo, LabelInfo } from "./WebApi";
import TeamList from "./TeamList";
import TeamInfo from "./TeamInfo";

const { Option } = Select;

interface MainPageState {
    stateStack: String[];
    teams: FullTeamInfo[];
    searchTeams: FullTeamInfo[];
    tagfilters: string[];
    alltags: LabelInfo[];
    teamid: string;
}

export interface MainPageProps {
    teamid?: string;
    searchTeams?: FullTeamInfo[];
}

class MainPage extends React.Component<MainPageProps, MainPageState> {
    constructor(props: MainPageProps) {
        super(props);
        console.log("mainpage constructor", this.props);
        const stack = props.teamid === undefined ? (props.searchTeams === undefined ? ['主页'] : ['主页', '搜索结果']) : ['主页', '队伍详情'];
        this.state = {
            stateStack: stack,
            teams: [],
            tagfilters: [],
            alltags: [],
            searchTeams: props.searchTeams === undefined ? [] : props.searchTeams,
            teamid: props.teamid === undefined ? '' : props.teamid
        };
        console.log(this.state);
        this.getAll();

        const tag_request = new Request("label/findAll", {
            cache: "reload",
        });
        fetch(tag_request)
            .then(
                (response: Response) => {
                    return response.json();
                },
                (reason: any) => {
                    message.error("获取标签失败：网络异常！");
                    console.log(reason);
                }
            )
            .then(
                (response: any) => {
                    console.log("获取标签成功" + response);
                    this.setState({
                        alltags: response,
                    });
                },
                (reason: any) => {
                    message.error("获取标签失败：服务器异常！");
                    console.log(reason);
                }
            );
    }

    componentWillReceiveProps(nextProps: MainPageProps) {
        this.setState({
            stateStack: nextProps.teamid === undefined ? (nextProps.searchTeams === undefined ? ['主页'] : ['主页', '搜索结果']) : ['主页', '队伍详情'],
            searchTeams: nextProps.searchTeams === undefined ? [] : nextProps.searchTeams,
            teamid: nextProps.teamid === undefined ? '' : nextProps.teamid,
        });
    }

    getAll() {
        console.log("get all teams!");
        const team_request = new Request("team/findAll", {
            cache: "reload",
        });
        fetch(team_request)
            .then(
                (response: Response) => {
                    return response.json();
                },
                (reason: any) => {
                    message.error("获取队伍列表失败：网络异常！");
                    console.log(reason);
                }
            )
            .then(
                (response: any) => {
                    console.log("获取队伍列表成功" + response);
                    this.setState({
                        teams: response,
                    });
                    console.log(this.state);
                },
                (reason: any) => {
                    message.error("获取队伍列表失败：服务器异常！");
                    console.log(reason);
                }
            );
    }

    toTeam(id: string) {
        let origin = this.state.stateStack;
        origin.push('队伍详情');
        this.setState({
            stateStack: origin,
            teamid: id
        });
    }

    handleRoute(page: String) {
        const origin = this.state.stateStack;
        let new_state: String[] = [];
        for (let i = 0; i < origin.length; ++i) {
            new_state.push(origin[i]);
            if (origin[i] === page)
                break;
        }
        this.setState({
            stateStack: new_state,
        });
    }

    handleFilterChange(labels: string[]) {
        this.setState({
            tagfilters: labels
        });
    }

    filterpred(team: FullTeamInfo) {
        const teamlabels = team.labels.map((t) => { return t.id; });
        console.log('filtering', team);
        console.log(this.state.tagfilters);
        console.log(teamlabels);
        for (const tag of this.state.tagfilters) {
            let result = false;
            for (const label of teamlabels) {
                if (tag === label) {
                    console.log('tag', tag, 'equals to', label);
                    result = true;
                }
            }
            if (!result) {
                return false
            }
        }
        return true;
    }

    render() {
        console.log("mainpage render", this.props, this.state);
        const tags = this.state.alltags.map((value) => {
            return <Option value={value.id}>{value.labelname}</Option>;
        });
        const current = this.state.stateStack[this.state.stateStack.length - 1];
        console.log(current);
        let content: React.ReactNode;
        if (current === '主页') {
            content = (
                <>
                    <Select
                        mode="tags"
                        placeholder="请筛选您想要的标签"
                        onChange={(value) => {
                            this.handleFilterChange(value);
                        }}
                        value={this.state.tagfilters}
                        style={{ width: "100%" }}
                    >
                        {tags}
                    </Select>
                    <TeamList
                        data={this.state.teams.filter((team) => { return this.filterpred(team); })}
                        toTeam={(id) => {
                            this.toTeam(id);
                        }}
                    ></TeamList>
                </>
            );
        } else if (current === '搜索结果') {
            content = (
                <>
                    <Select
                        mode="tags"
                        placeholder="请筛选您想要的标签"
                        onChange={(value) => {
                            this.handleFilterChange(value);
                        }}
                        value={this.state.tagfilters}
                        style={{ width: "100%" }}
                    >
                        {tags}
                    </Select>
                    <TeamList
                        data={this.state.searchTeams.filter((team) => { return this.filterpred(team); })}
                        toTeam={(id) => {
                            this.toTeam(id);
                        }}
                    ></TeamList>
                </>
            );
        } else if (current === '队伍详情') {
            content = (
                <TeamInfo id={this.state.teamid} />
            );
        }
        return (
            <Layout>
                <Row>
                    <Col offset={4} span={16}>
                        <Breadcrumb>
                            {this.state.stateStack.map((page, index) => { return <Breadcrumb.Item><a onClick={() => { this.handleRoute(page); }}>{page}</a></Breadcrumb.Item>; })}
                        </Breadcrumb>
                    </Col>
                </Row>
                <Row>
                    <Col offset={4} span={16}>
                        {content}
                    </Col>
                </Row>
            </Layout>
        )
    }
}

export default MainPage;