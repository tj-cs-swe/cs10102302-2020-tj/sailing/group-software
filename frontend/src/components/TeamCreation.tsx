import React from "react";
import {
    Button,
    Typography,
    Input,
    DatePicker,
    TimePicker,
    Divider,
    Select,
    message,
} from "antd";
import moment from "moment";
import { BasicTeamInfo, LabelInfo } from "./WebApi";

const { Title, Paragraph } = Typography;
const { Option } = Select;

interface TeamCreationProps {
    backHome(): void;
}

interface TeamCreationStates {
    teaminfo: BasicTeamInfo;
    date: string;
    time: string;
    selected_tags: string[];
    all_tags: LabelInfo[];
}

class TeamCreation extends React.Component<
    TeamCreationProps,
    TeamCreationStates
    > {
    constructor(props: TeamCreationProps) {
        super(props);
        const today = moment().format("YYYY-MM-DD");
        this.state = {
            date: today,
            time: "23:59:59",
            teaminfo: {
                teamname: "",
                description: "",
                requirements: "",
                deadline: today + " " + "23:59:59",
            },
            selected_tags: [],
            all_tags: [],
        };
        const tag_request = new Request("label/findAll", {
            cache: "reload",
        });
        fetch(tag_request)
            .then(
                (response: Response) => {
                    return response.json();
                },
                (reason: any) => {
                    message.error("获取标签失败：网络异常！");
                    console.log(reason);
                }
            )
            .then(
                (response: any) => {
                    console.log("获取标签成功" + response);
                    this.setState({
                        all_tags: response, //may have problem
                    });
                },
                (reason: any) => {
                    message.error("获取标签失败：服务器异常！");
                    console.log(reason);
                }
            );
    }

    handleClick() {
        const create_request = new Request(`team/create`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json; charset=utf-8",
            },
            body: JSON.stringify({
                team: this.state.teaminfo,
                label_ids: this.state.selected_tags,
            }),
            cache: "reload",
        });
        fetch(create_request)
            .then(
                (response: Response) => {
                    return response.json();
                },
                (reason: any) => {
                    message.error("创建失败：网络异常！");
                    console.log(reason);
                }
            )
            .then(
                (response: any) => {
                    if (response.status === false) {
                        message.error(`创建失败： ${response.msg}`);
                    } else {
                        message.success(response.msg);
                        console.log("创建成功" + response);
                        console.log("接下来应该调用backHome函数")
                        this.props.backHome();
                    }
                },
                (reason: any) => {
                    message.error("创建失败：服务器异常！");
                    console.log(reason);
                }
            );
        this.props.backHome();
    }

    handleTitleChange(event: React.ChangeEvent<HTMLInputElement>) {
        console.log(event);
        let new_teaminfo = this.state.teaminfo;
        new_teaminfo.teamname = event.target.value;
        this.setState({
            teaminfo: new_teaminfo
        });
    }

    handleDescriptionChange(event: React.ChangeEvent<HTMLTextAreaElement>) {
        console.log(event);
        let new_teaminfo = this.state.teaminfo;
        new_teaminfo.description = event.target.value;
        this.setState({
            teaminfo: new_teaminfo
        });
    }

    handleRequirementsChange(event: React.ChangeEvent<HTMLTextAreaElement>) {
        console.log(event);
        let new_teaminfo = this.state.teaminfo;
        new_teaminfo.requirements = event.target.value;
        this.setState({
            teaminfo: new_teaminfo
        });
    }

    handleDateChange(date: any, dateString: string) {
        console.log(date, dateString);
        let new_date = date.format("YYYY-MM-DD")
        let new_teaminfo = this.state.teaminfo;
        new_teaminfo.deadline = new_date + " " + this.state.time;
        this.setState({
            teaminfo: new_teaminfo,
            date: new_date
        });
    }

    handleTimeChange(time: any, timeString: string) {
        console.log(time, timeString);
        let new_time = time.format("HH:mm:ss")
        let new_teaminfo = this.state.teaminfo;
        new_teaminfo.deadline = this.state.date + " " + new_time;
        this.setState({
            teaminfo: new_teaminfo,
            time: new_time
        });
    }

    handleTagsChange(value: any) {
        console.log(value);
        this.setState({
            selected_tags: value,
        });
    }

    render(): React.ReactNode {
        const tags = this.state.all_tags.map((value, index, array) => {
            return <Option value={value.id}>{value.labelname}</Option>;
        });
        return (
            <Typography>
                <Title level={2}>创建队伍</Title>
                <Divider />
                <Paragraph>
                    <Title level={4}>队伍名称</Title>
                    <Input onChange={(e) => { this.handleTitleChange(e) }} />
                </Paragraph>
                <Divider />
                <Paragraph>
                    <Title level={4}>队伍描述</Title>
                    <Input.TextArea onChange={(e) => { this.handleDescriptionChange(e) }} />
                </Paragraph>
                <Divider />
                <Paragraph>
                    <Title level={4}>人员要求</Title>
                    <Input.TextArea onChange={(e) => { this.handleRequirementsChange(e) }} />
                </Paragraph>
                <Divider />
                <Paragraph>
                    <Title level={4}>截止日期</Title>
                    <DatePicker
                        onChange={(date, dateString) => { this.handleDateChange(date, dateString) }}
                        defaultValue={moment(this.state.date, "YYYY/MM/DD")}
                    />
                    <TimePicker
                        onChange={(time, timeString) => { this.handleTimeChange(time, timeString) }}
                        defaultValue={moment(this.state.time, "HH:mm:SS")}
                    />
                </Paragraph>
                <Divider />
                <Title level={4}>选择标签</Title>
                <Select
                    mode="tags"
                    placeholder="Please select"
                    onChange={(value) => { this.handleTagsChange(value) }}
                    style={{ width: '100%' }}
                >
                    {tags}
                </Select>
                <Divider />
                <Button type="primary" onClick={(e) => { this.handleClick() }}>
                    创建队伍
                </Button>
            </Typography>
        );
    }
}

export default TeamCreation;
