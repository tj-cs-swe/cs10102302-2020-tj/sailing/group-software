package com.tongji.service.impl;

import com.tongji.dao.LabelDao;
import com.tongji.domain.Label;
import com.tongji.service.LabelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LabelServiceImpl implements LabelService {

    @Autowired
    LabelDao labelDao;

    //查找所有标签
    @Override
    public List<Label> findAll() {
        return labelDao.findAll();
    }


    /**
     * 根据队伍id查找队伍所拥有的标签
     * @param teamId
     * @return
     */
    @Override
    public List<Label> findTeamLabel(String teamId) {
        return labelDao.findByTeamId(teamId);
    }

    /**
     * 查找不属于队伍的标签
     * @param teamId teamid
     * @return label
     */
    @Override
    public List<Label> findOther(String teamId) {
        return labelDao.findOtherLabel(teamId);
    }


}
