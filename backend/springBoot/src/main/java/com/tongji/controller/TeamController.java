package com.tongji.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.tongji.domain.ReInfo;
import com.tongji.domain.Team;
import com.tongji.service.TeamService;
import com.tongji.util.Utils;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.beanutils.ConvertUtils;
import org.apache.commons.beanutils.converters.DateConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/team")
public class TeamController {

    @Autowired
    TeamService teamService;


    //创建队伍
    //需要将team对象和标签列表封装成一个json对象传送过来
    @RequestMapping("/create")
    @ResponseBody
    public ReInfo createTeam(@RequestBody Map<String,Object>map, HttpServletRequest request) throws InvocationTargetException, IllegalAccessException, ParseException {
        Team team=new Team();

        DateConverter converter = new DateConverter();
        converter.setPattern(new String("yyyy-MM-dd HH:mm:ss"));
        ConvertUtils.register(converter, Date.class);

        BeanUtils.populate(team, (Map<String, ? extends Object>) map.get("team"));//封装队伍对象

       /* SimpleDateFormat sm=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        team.setDeadline(sm.parse((String) ((Map<String, ? extends Object>) map.get("team")).get("date")));*/

        List<String>label_ids= (List<String>) map.get("label_ids");//相关标签ID
        String uid = Utils.getUid(request.getCookies());//获得创建者的ID
        team.setUid(uid);
        ReInfo info = teamService.createTeam(team, label_ids);
        return info;
    }

    //更新队伍信息
    //需要将team对象和标签列表封装成一个json对象传送过来
    @RequestMapping("/update")
    @ResponseBody
    public String updateTeam(@RequestBody Map<String,Object>map) throws InvocationTargetException, IllegalAccessException {
        Team team=new Team();

        DateConverter converter = new DateConverter();
        converter.setPattern(new String("yyyy-MM-dd HH:mm:ss"));
        ConvertUtils.register(converter, Date.class);

        BeanUtils.populate(team, (Map<String, ? extends Object>) map.get("team"));//封装队伍对象

        List<String>label_ids= (List<String>) map.get("label_ids");//相关标签ID
        teamService.updateTeam(team,label_ids);
        return "修改队伍信息成功";
    }

    //查找队伍详情
    @RequestMapping("/findDetail")
    @ResponseBody
    public Team findDetail(@RequestBody Map<String,Object>map){
        return teamService.findById((String) map.get("teamId"));
    }


    /**
     * 查询所有队伍
     */
    @RequestMapping("/findAll")
    @ResponseBody
    public List<Team> findAll(){
        List<Team> all = teamService.findAll();
        return all;
    }

    /**
     * 查询关键词队伍
     */
    @RequestMapping("/findByWord")
    @ResponseBody
    public List<Team>findByWord(@RequestBody Map<String,Object>map){
        List<Team> byWord = teamService.findByWord((String) map.get("word"));
        return byWord;
    }

    /**
     * 根据标签进行分页查询
     *
     */
    @RequestMapping("/findByLabel")
    @ResponseBody
    public List<Team> findByLabel(@RequestBody Map<String,Object>map){
        List<Team> bylabel = teamService.findByLabel((List<String>) map.get("label_ids"));
        return bylabel;
    }


    //--------------------------------------------------------------------------

    /**
     * 查找正在申请的队伍
     * @param
     * @return
     */
    @RequestMapping("/findApply")
    @ResponseBody
    public List<Team> findApplyTeam(HttpServletRequest request){
        return teamService.findApplyTeam(Utils.getUid(request.getCookies()));
    }

    /**
     * 查找已经加入的队伍
     * @param
     * @return
     */
    @RequestMapping("/findJoin")
    @ResponseBody
    public List<Team> findJoinTeam(HttpServletRequest request){
        return teamService.findJoinTeam(Utils.getUid(request.getCookies()));
    }


    /**
     * 查找我创建的队伍
     * @param
     * @return
     */
    @RequestMapping("/findCreate")
    @ResponseBody
    public List<Team> findCreateTeam(HttpServletRequest request){
        return teamService.findCreateTeam(Utils.getUid(request.getCookies()));
    }


    //申请加入队伍
    @RequestMapping("/applyTeam")
    @ResponseBody
    public String applyTeam(@RequestBody Map<String,Object>map,HttpServletRequest request){
        teamService.applyTeam(Utils.getUid(request.getCookies()),(String) map.get("teamid"));
        return "申请成功，等待队伍创建人确认!";
    }


    //拒绝加入申请
    @RequestMapping("/rejectApply")
    @ResponseBody
    public void rejectApply(@RequestBody Map<String,Object>map){
        teamService.rejectApply((String) map.get("uid"),(String) map.get("teamid"));
    }

    //同意加入申请
    @RequestMapping("/joinTeam")
    @ResponseBody
    public void joinTeam(@RequestBody Map<String,Object>map){
        teamService.joinTeam((String) map.get("uid"),(String) map.get("teamid"));
    }


    //取消申请
    @RequestMapping("/quitApply")
    @ResponseBody
    public String quitApply(@RequestBody Map<String,Object>map){
        teamService.quitApply((String) map.get("uid"),(String) map.get("teamid"));
        return "取消申请加入队伍成功!";
    }



    //申请退出队伍
    @RequestMapping("/applyDropTeam")
    @ResponseBody
    public String applyDropTeam(@RequestBody Map<String,Object>map,HttpServletRequest request){
        teamService.applyDropTeam(Utils.getUid(request.getCookies()),(String) map.get("teamid"));
        return "申请成功，等待队伍创建人确认!";
    }


    //拒绝退出申请
    @RequestMapping("/rejectDrop")
    @ResponseBody
    public void rejectDrop(@RequestBody Map<String,Object>map){
        teamService.rejectDrop((String) map.get("uid"),(String) map.get("teamid"));
    }

    //同意退出申请
    @RequestMapping("/dropTeam")
    @ResponseBody
    public void dropTeam(@RequestBody Map<String,Object>map){
        teamService.dropTeam((String) map.get("uid"),(String) map.get("teamid"));
    }

    //取消退出
    @RequestMapping("/quitDrop")
    @ResponseBody
    public String quitDrop(@RequestBody Map<String,Object>map){
        teamService.quitDrop((String) map.get("uid"),(String) map.get("teamid"));

        return "取消申请退出队伍成功!";
    }


    //完成组队
    @RequestMapping("/completeTeam")
    @ResponseBody
    public ReInfo completeTeam(@RequestBody Map<String,Object>map){

        ReInfo reInfo = teamService.completeTeam((String) map.get("teamid"));
        return reInfo;

    }

    //解散队伍
    @RequestMapping("/breakTeam")
    @ResponseBody
    public String breakTeam(@RequestBody Map<String,Object>map){
        teamService.breakTeam((String) map.get("teamid"));
        return "解散队伍成功";
    }



}
