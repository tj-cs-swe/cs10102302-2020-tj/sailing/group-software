import * as React from "react";
import { message, Form, Input, Button } from "antd";

const layout = {
    labelCol: { span: 8 },
    wrapperCol: { span: 16 },
};

const tailLayout = {
    wrapperCol: { offset: 8, span: 16 },
};

interface LoginFormProps {
    onLogin(username: string, failed: boolean): void;
}

const LoginForm = (props: LoginFormProps) => {
    const onFinish = (values: any) => {
        const login_request = new Request(
            `user/login?username=${values.username}&password=${values.password}`,
            {
                cache: "reload"
            }
        );
        fetch(login_request)
            .then(
                (response: Response) => {
                    return response.json();
                },
                (reason: any) => {
                    message.error("登录失败：网络异常！");
                    props.onLogin("", true);
                    console.log(reason);
                }
            )
            .then(
                (response: any) => {
                    if (response.status === true) {
                        console.log("登陆成功", response);
                        props.onLogin(values.username, false);
                    } else {
                        console.log("登陆失败", response);
                        message.error(`登录失败： ${response.msg}`);
                        props.onLogin("", true);
                    }
                },
                (reason: any) => {
                    message.error("登录失败：服务器异常！");
                    props.onLogin("", true);
                    console.log(reason);
                }
            );
    };

    return (
        <Form
            {...layout}
            name="basic"
            initialValues={{ remember: true }}
            onFinish={onFinish}
        >
            <Form.Item
                label="用户名"
                name="username"
                rules={[
                    { required: true, message: "Please input your username!" },
                ]}
            >
                <Input />
            </Form.Item>

            <Form.Item
                label="密码"
                name="password"
                rules={[
                    { required: true, message: "Please input your password!" },
                ]}
            >
                <Input.Password />
            </Form.Item>

            <Form.Item {...tailLayout}>
                <Button type="primary" htmlType="submit">
                    登录
                </Button>
            </Form.Item>
        </Form>
    );
};

export default LoginForm;
