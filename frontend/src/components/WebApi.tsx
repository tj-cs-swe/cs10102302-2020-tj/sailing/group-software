import { message } from 'antd';

export interface User {
    id: string;
    username: string;
    password: string;
    nickname: string;
    sex: string;
    phonenum: string;
    email: string;
}

export interface BasicTeamInfo {
    teamname: string,
    description: string,
    requirements: string,
    deadline: string
}

export interface LabelInfo {
    id: string,
    labelname: string
}

export interface FullTeamInfo {
    status: boolean;
    id: string,
    uid: string,
    teamname: string,
    description: string,
    requirements: string,
    deadline: string,
    number: number,
    labels: LabelInfo[]
}

export interface ReInfo {
    status: boolean;
    msg: string;
}

export interface Logs {
    id: string;
    uid: string;
    msg: string;
    ltime: string;
    status: boolean;
}

export async function findApply() {
    const find_request = new Request("team/findApply", {
        cache: "reload",
    });
    try {
        const response = await fetch(find_request);
        const data = await response.json()
        return data as FullTeamInfo[];
    }
    catch (err) {
        message.error("获取已申请队伍列表失败");
        console.log("error: ", err);
        return [];
    }
}

export async function findJoin() {
    const find_request = new Request("team/findJoin", {
        cache: "reload",
    });
    try {
        const response = await fetch(find_request);
        const data = await response.json()
        return data as FullTeamInfo[];
    }
    catch (err) {
        message.error("获取已加入队伍列表失败");
        console.log("error: ", err);
        return [];
    }
}

export async function findCreate() {
    const find_request = new Request("team/findCreate", {
        cache: "reload",
    });
    try {
        const response = await fetch(find_request);
        const data = await response.json()
        return data as FullTeamInfo[];
    }
    catch (err) {
        message.error("获取已创建队伍列表失败");
        console.log("error: ", err);
        return [];
    }
}

export async function applyTeam(teamid: string) {
    const request = new Request("team/applyTeam", {
        method: "POST",
        headers: {
            "Content-Type": "application/json; charset=utf-8",
        },
        body: JSON.stringify({
            teamid: teamid,
        }),
        cache: "reload",
    });

    try {
        const response = await fetch(request);
        const data = await response.text();
        return data as string;
    }
    catch (err) {
        message.error("申请加入队伍失败");
        console.log("error: ", err);
        return "申请加入队伍失败";
    }
}

export async function quitApply(teamid: string) {
    const request = new Request("team/quitApply", {
        method: "POST",
        headers: {
            "Content-Type": "application/json; charset=utf-8",
        },
        body: JSON.stringify({
            teamid: teamid,
        }),
        cache: "reload",
    });

    try {
        const response = await fetch(request);
        const data = await response.text();
        return data as string;
    }
    catch (err) {
        message.error("取消加入请求失败");
        console.log("error: ", err);
        return "取消加入请求失败";
    }
}

export async function rejectApply(teamid: string, uid: string) {
    const request = new Request("team/rejectApply", {
        method: "POST",
        headers: {
            "Content-Type": "application/json; charset=utf-8",
        },
        body: JSON.stringify({
            teamid: teamid,
            uid: uid,
        }),
        cache: "reload",
    });

    try {
        await fetch(request);
    }
    catch (err) {
        message.error("拒绝申请失败：网络错误！");
        console.log("error: ", err);
    }
}

export async function joinTeam(teamid: string, uid: string) {
    const request = new Request("team/joinTeam", {
        method: "POST",
        headers: {
            "Content-Type": "application/json; charset=utf-8",
        },
        body: JSON.stringify({
            teamid: teamid,
            uid: uid,
        }),
        cache: "reload",
    });

    try {
        await fetch(request);
    }
    catch (err) {
        message.error("同意申请失败：网络错误！");
        console.log("error: ", err);
    }
}

export async function applyDropTeam(teamid: string) {
    const request = new Request("team/applyDropTeam", {
        method: "POST",
        headers: {
            "Content-Type": "application/json; charset=utf-8",
        },
        body: JSON.stringify({
            teamid: teamid,
        }),
        cache: "reload",
    });

    try {
        const response = await fetch(request);
        const data = await response.text();
        return data as string;
    }
    catch (err) {
        message.error("申请退出队伍失败");
        console.log("error: ", err);
        return "申请退出队伍失败";
    }
}

export async function quitDrop(teamid: string) {
    const request = new Request("team/quitDrop", {
        method: "POST",
        headers: {
            "Content-Type": "application/json; charset=utf-8",
        },
        body: JSON.stringify({
            teamid: teamid,
        }),
        cache: "reload",
    });

    try {
        const response = await fetch(request);
        const data = await response.text();
        return data as string;
    }
    catch (err) {
        message.error("取消退出队伍失败");
        console.log("error: ", err);
        return "取消退出队伍失败";
    }
}

export async function rejectDrop(teamid: string, uid: string) {
    const request = new Request("team/rejectDrop", {
        method: "POST",
        headers: {
            "Content-Type": "application/json; charset=utf-8",
        },
        body: JSON.stringify({
            teamid: teamid,
            uid: uid,
        }),
        cache: "reload",
    });

    try {
        await fetch(request);
    }
    catch (err) {
        message.error("拒绝退出申请失败：网络错误！");
        console.log("error: ", err);
    }
}

export async function dropTeam(teamid: string, uid: string) {
    const request = new Request("team/dropTeam", {
        method: "POST",
        headers: {
            "Content-Type": "application/json; charset=utf-8",
        },
        body: JSON.stringify({
            teamid: teamid,
            uid: uid,
        }),
        cache: "reload",
    });

    try {
        await fetch(request);
    }
    catch (err) {
        message.error("同意退出申请失败：网络错误！");
        console.log("error: ", err);
    }
}

export async function completeTeam(teamid: string) {
    const request = new Request("team/completeTeam", {
        method: "POST",
        headers: {
            "Content-Type": "application/json; charset=utf-8",
        },
        body: JSON.stringify({
            teamid: teamid,
        }),
        cache: "reload",
    });

    try {
        const response = await fetch(request);
        const data = await response.json();
        return data as ReInfo;
    }
    catch (err) {
        message.error("完成组队失败");
        console.log("error: ", err);
        return { status: false, msg: "完成组队失败" } as ReInfo
    }
}

export async function breakTeam(teamid: string) {
    const request = new Request("team/breakTeam", {
        method: "POST",
        headers: {
            "Content-Type": "application/json; charset=utf-8",
        },
        body: JSON.stringify({
            teamid: teamid,
        }),
        cache: "reload",
    });

    try {
        const response = await fetch(request);
        const data = await response.text();
        return data as string;
    }
    catch (err) {
        message.error("解散队伍失败");
        console.log("error: ", err);
        return "解散队伍失败";
    }
}

export async function findCreateMember(teamid: string) {
    const request = new Request("user/findCreateMember", {
        method: "POST",
        headers: {
            "Content-Type": "application/json; charset=utf-8",
        },
        body: JSON.stringify({
            teamid: teamid,
        }),
        cache: "reload",
    });

    const response = await fetch(request);
    const data = await response.json();
    return data as User;
}

export async function findApplyMember(teamid: string) {
    const request = new Request("user/findApplyMember", {
        method: "POST",
        headers: {
            "Content-Type": "application/json; charset=utf-8",
        },
        body: JSON.stringify({
            teamid: teamid,
        }),
        cache: "reload",
    });

    try {
        const response = await fetch(request);
        const data = await response.json();
        return data as User[];
    }
    catch (err) {
        message.error("查找队伍成员信息失败");
        console.log("error: ", err);
        return [];
    }
}

export async function findTeamMember(teamid: string) {
    const request = new Request("user/findTeamMember", {
        method: "POST",
        headers: {
            "Content-Type": "application/json; charset=utf-8",
        },
        body: JSON.stringify({
            teamid: teamid,
        }),
        cache: "reload",
    });

    try {
        const response = await fetch(request);
        const data = await response.json();
        return data as User[];
    }
    catch (err) {
        message.error("查找队伍成员信息失败");
        console.log("error: ", err);
        return [];
    }
}

export async function findDropMember(teamid: string) {
    const request = new Request("user/findDropMember", {
        method: "POST",
        headers: {
            "Content-Type": "application/json; charset=utf-8",
        },
        body: JSON.stringify({
            teamid: teamid,
        }),
        cache: "reload",
    });

    try {
        const response = await fetch(request);
        const data = await response.json();
        return data as User[];
    }
    catch (err) {
        message.error("查找申请退出队伍成员信息失败");
        console.log("error: ", err);
        return [];
    }
}

export async function findUnread() {
    const find_request = new Request("logs/findUnread", {
        cache: "reload",
    });
    try {
        const response = await fetch(find_request);
        const data = await response.json()
        return data as Logs[];
    }
    catch (err) {
        message.error("获取未读信息列表失败");
        console.log("error: ", err);
        return [];
    }
}

export async function findread() {
    const find_request = new Request("logs/findread", {
        cache: "reload",
    });
    try {
        const response = await fetch(find_request);
        const data = await response.json()
        return data as Logs[];
    }
    catch (err) {
        message.error("获取已读信息列表失败");
        console.log("error: ", err);
        return [];
    }
}

export async function confirm(id: string) {
    const request = new Request("logs/confirm", {
        method: "POST",
        headers: {
            "Content-Type": "application/json; charset=utf-8",
        },
        body: JSON.stringify({
            id: id,
        }),
        cache: "reload",
    });

    try {
        await fetch(request);
    }
    catch (err) {
        message.error("确认消息失败：网络错误！");
        console.log("error: ", err);
    }
}