import React from "react";
import { Card } from "antd";
import { User, FullTeamInfo } from "./WebApi";

export interface RequestInfo {
    team: FullTeamInfo;
    user: User;
}

interface MsgViewProps {
    data: RequestInfo[]
    type: "join" | "quit"
    onFinish(teamid:string, id: string): void;
    onCancel(teamid:string, id: string): void;
}

interface MsgViewStates {
}

class MsgView extends React.Component<MsgViewProps, MsgViewStates> {
    constructor(props: MsgViewProps) {
        super(props);
    }

    render(): React.ReactNode {
        console.log("Msg List Render");
        if (this.props.type === "join") {
            return this.props.data.map((t) => {
                return (
                    <Card
                        title="加入申请"
                        actions={[
                        <a onClick={() => { this.props.onFinish(t.team.id, t.user.id) }}>同意</a>,
                        <a onClick={() => { this.props.onCancel(t.team.id, t.user.id) }}>拒绝</a>]}>
                        {t.user.nickname} 申请加入您的队伍 {t.team.teamname}
                    </Card>
                );
            });
        } else {
            return this.props.data.map((t) => {
                return (
                    <Card
                        title="退出申请"
                        actions={[
                            <a onClick={() => { this.props.onFinish(t.team.id, t.user.id) }}>同意</a>,
                            <a onClick={() => { this.props.onCancel(t.team.id, t.user.id) }}>拒绝</a>]}>
                            {t.user.nickname} 申请退出您的队伍 {t.team.teamname}
                    </Card>
                );
            });
        }
    }
}

export default MsgView;
