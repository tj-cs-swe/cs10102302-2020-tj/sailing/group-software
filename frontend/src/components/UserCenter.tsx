import * as React from "react";
import { message, List, Layout, Menu, Row, Col, Card } from "antd";
import { ClickParam } from "antd/es/menu";

import InfoForm, { UserInfo } from "./InfoForm";
import TeamCreation from './TeamCreation';
import TeamView from "./TeamView";
import MsgView, { RequestInfo } from "./MsgView";
import { FullTeamInfo, findCreate, findApply, findJoin, completeTeam, breakTeam, applyDropTeam, joinTeam, rejectApply, dropTeam, rejectDrop, findTeamMember, findDropMember, findApplyMember, Logs, findUnread, confirm, quitApply } from "./WebApi";
import InfoView from "./InfoView";

const { Sider } = Layout;

interface UserCenterProps {
    onLogout(): void;
    backHome(): void;
    toTeam(id: string): void;
}

interface UserCenterStates {
    current: string;
    userinfo: UserInfo;
    teamviews: FullTeamInfo[];
    msgviews: RequestInfo[];
    infoviews: Logs[];
}

async function getinfo(): Promise<UserInfo> {
    const user_request = new Request("/user/myInfo");
    const response = await fetch(user_request);
    return response.json();
}

class UserCenter extends React.Component<UserCenterProps, UserCenterStates> {
    constructor(props: UserCenterProps) {
        super(props);
        console.log('user props:', props)
        let info: UserInfo = {
            username: "null",
            password: "null",
            nickname: "null",
            sex: "null",
            phonenum: "null",
            email: "null",
        };
        this.state = {
            current: "userinfo",
            userinfo: info,
            teamviews: [],
            msgviews: [],
            infoviews: [],
        };
        getinfo().then((userinfo: UserInfo) => {
            info = userinfo;
            this.setState({
                userinfo: userinfo,
            });
        });
    }

    handleClick(clickEvent: ClickParam) {
        if (clickEvent.key === "userinfo") {
            getinfo().then((userinfo: UserInfo) => {
                this.setState({
                    current: "userinfo",
                    userinfo: userinfo,
                });
            }, (reason: any) => {
                console.log("error:", reason);
            });
            this.setState({
                current: "userinfo"
            });
        } else if (clickEvent.key === "editinfo") {
            this.setState({
                current: "editinfo",
            });
        } else if (clickEvent.key === "createteam") {
            this.setState({
                current: "createteam",
            });
        } else if (clickEvent.key === "created") {
            findCreate().then((teams) => {
                this.setState({
                    current: "created",
                    teamviews: teams
                })
            })
            this.setState({
                current: "created",
            });
        } else if (clickEvent.key === "requested") {
            findApply().then((teams) => {
                this.setState({
                    current: "requested",
                    teamviews: teams
                })
            })
            this.setState({
                current: "requested",
            });
        } else if (clickEvent.key === "joined") {
            findJoin().then((teams) => {
                this.setState({
                    current: "joined",
                    teamviews: teams
                })
            })
            this.setState({
                current: "joined",
            });
        } else if (clickEvent.key === "joinmsgs") {
            (async () => {
                let result: RequestInfo[] = []
                const teams = await findCreate()
                for (const team of teams) {
                    const users = await findApplyMember(team.id);
                    for (const user of users) {
                        result.push({ team: team, user: user } as RequestInfo);
                    }
                }
                this.setState({
                    current: "joinmsgs",
                    msgviews: result,
                });
            })()
            this.setState({
                current: "joingmsgs",
            });
        } else if (clickEvent.key === "quitmsgs") {
            (async () => {
                let result: RequestInfo[] = []
                const teams = await findCreate()
                for (const team of teams) {
                    const users = await findDropMember(team.id);
                    for (const user of users) {
                        result.push({ team: team, user: user } as RequestInfo);
                    }
                }
                this.setState({
                    current: "quitmsgs",
                    msgviews: result,
                });
            })()
            this.setState({
                current: "quitmsgs",
            });
        } else if (clickEvent.key === "infomsgs") {
            (async () => {
                const result = await findUnread()
                this.setState({
                    current: "infomsgs",
                    infoviews: result,
                });
            })()
            this.setState({
                current: "infomsgs",
            });
        } else {
            (async () => {
                const logout_request = new Request("/user/loginOut");
                fetch(logout_request);
                document.cookie = "";
                this.props.onLogout();
            })();
        }
    }

    userinfo(): React.ReactNode {
        const data = [
            {
                title: "用户名",
                value: this.state.userinfo.username,
            },
            {
                title: "昵称",
                value: this.state.userinfo.nickname,
            },
            {
                title: "性别",
                value: this.state.userinfo.sex,
            },
            {
                title: "电话号码",
                value: this.state.userinfo.phonenum,
            },
            {
                title: "电子邮箱",
                value: this.state.userinfo.email,
            },
        ];
        return (
            <List
                itemLayout="horizontal"
                dataSource={data}
                renderItem={(item: any) => (
                    <List.Item>
                        <List.Item.Meta
                            title={item.title}
                            description={item.value}
                        />
                    </List.Item>
                )}
            />
        );
    }

    handleComplete(teamid: string) {
        completeTeam(teamid).then((reinfo) => {
            if (reinfo.status) {
                message.success("完成组队成功");
                findCreate().then((teams) => {
                    console.log("handleComplete refresh setState", teams)
                    this.setState({
                        current: "created",
                        teamviews: teams
                    })
                })
                this.setState({
                    current: "created",
                });
            }
            else {
                message.warning("组队失败" + reinfo.msg);
            }
        })
    }

    handleDelete(teamid: string) {
        breakTeam(teamid).then((msg) => {
            message.info(msg);
            findCreate().then((teams) => {
                this.setState({
                    current: "created",
                    teamviews: teams
                })
            })
            this.setState({
                current: "created",
            });
        })
    }

    handleCancel(teamid: string) {
        quitApply(teamid).then((msg) => {
            message.info(msg);
            findApply().then((teams) => {
                this.setState({
                    current: "requested",
                    teamviews: teams
                })
            })
            this.setState({
                current: "requested",
            });
        })
    }

    handleQuit(teamid: string) {
        applyDropTeam(teamid).then((msg) => {
            message.info(msg);
            findJoin().then((teams) => {
                this.setState({
                    current: "joined",
                    teamviews: teams
                })
            })
            this.setState({
                current: "joined",
            });
        })
    }

    acceptRequest(teamid: string, uid: string) {
        joinTeam(teamid, uid).then(() => {
            message.success("同意申请成功");
            (async () => {
                let result: RequestInfo[] = []
                const teams = await findCreate()
                for (const team of teams) {
                    const users = await findApplyMember(team.id);
                    for (const user of users) {
                        result.push({ team: team, user: user } as RequestInfo);
                    }
                }
                this.setState({
                    current: "joinmsgs",
                    msgviews: result,
                });
            })()
            this.setState({
                current: "joingmsgs",
            });
        })
    }

    rejectRequest(teamid: string, uid: string) {
        rejectApply(teamid, uid).then(() => {
            message.success("拒绝申请成功");
            (async () => {
                let result: RequestInfo[] = []
                const teams = await findCreate()
                for (const team of teams) {
                    const users = await findApplyMember(team.id);
                    for (const user of users) {
                        result.push({ team: team, user: user } as RequestInfo);
                    }
                }
                this.setState({
                    current: "joinmsgs",
                    msgviews: result,
                });
            })()
            this.setState({
                current: "joingmsgs",
            });
        })
    }

    acceptQuitRequest(teamid: string, uid: string) {
        dropTeam(teamid, uid).then(() => {
            message.success("同意退出申请成功");
            (async () => {
                let result: RequestInfo[] = []
                const teams = await findCreate()
                for (const team of teams) {
                    const users = await findDropMember(team.id);
                    for (const user of users) {
                        result.push({ team: team, user: user } as RequestInfo);
                    }
                }
                this.setState({
                    current: "quitmsgs",
                    msgviews: result,
                });
            })()
            this.setState({
                current: "quitmsgs",
            });
        })
    }

    rejectQuitRequest(teamid: string, uid: string) {
        rejectDrop(teamid, uid).then(() => {
            message.success("拒绝退出申请成功");
            (async () => {
                let result: RequestInfo[] = []
                const teams = await findCreate()
                for (const team of teams) {
                    const users = await findDropMember(team.id);
                    for (const user of users) {
                        result.push({ team: team, user: user } as RequestInfo);
                    }
                }
                this.setState({
                    current: "quitmsgs",
                    msgviews: result,
                });
            })()
            this.setState({
                current: "quitmsgs",
            });
        })
    }

    render(): React.ReactNode {
        let content: React.ReactNode;
        if (this.state.current === "userinfo") {
            content = this.userinfo()
        } else if (this.state.current === "editinfo") {
            content = (
                <InfoForm
                    onSuccess={(info: string) => {
                        message.info(info);
                        getinfo().then((userinfo: UserInfo) => {
                            this.setState({
                                current: "userinfo",
                                userinfo: userinfo,
                            });
                        });
                    }}
                    defaultValues={this.state.userinfo}
                />
            )
        } else if (this.state.current === "createteam") {
            content = (
                <TeamCreation backHome={() => { this.props.backHome() }} />
            );
        } else if (this.state.current === "created") {
            content = (
                <TeamView
                    data={this.state.teamviews}
                    type="created"
                    toTeam={(id) => { this.props.toTeam(id); }}
                    onFinish={(id) => { this.handleComplete(id); }}
                    onCancel={(id) => { this.handleDelete(id); }}
                />
            );
        } else if (this.state.current === "requested") {
            content = (
                <TeamView
                    data={this.state.teamviews}
                    type="requested"
                    toTeam={(id) => { this.props.toTeam(id); }}
                    onFinish={(id) => { }}
                    onCancel={(id) => { this.handleCancel(id); }}
                />
            );
        } else if (this.state.current === "joined") {
            content = (
                <TeamView
                    data={this.state.teamviews}
                    type="joined"
                    toTeam={(id) => { this.props.toTeam(id); }}
                    onFinish={(id) => { }}
                    onCancel={(id) => { this.handleQuit(id); }}
                />
            );
        } else if (this.state.current === "joinmsgs") {
            content = (
                <MsgView
                    data={this.state.msgviews}
                    type="join"
                    onFinish={(teamid, id) => { this.acceptRequest(teamid, id); }}
                    onCancel={(teamid, id) => { this.rejectRequest(teamid, id); }}
                />
            )
        } else if (this.state.current === "quitmsgs") {
            content = (
                <MsgView
                    data={this.state.msgviews}
                    type="quit"
                    onFinish={(teamid, id) => { this.acceptQuitRequest(teamid, id); }}
                    onCancel={(teamid, id) => { this.rejectQuitRequest(teamid, id); }}
                />
            )
        } else if (this.state.current === "infomsgs") {
            content = (
                <InfoView
                    data={this.state.infoviews}
                    onFinish={(id) => {
                        (async () => {
                            await confirm(id);
                            const result = await findUnread()
                            this.setState({
                                current: "infomsgs",
                                infoviews: result,
                            });
                        })()
                        this.setState({
                            current: "infomsgs",
                        });
                    }}
                />
            )
        }
        return (
            <Layout>
                <Sider>
                    <Menu
                        className="vertical-menu"
                        mode="inline"
                        defaultSelectedKeys={["userinfo"]}
                        onClick={(clickEvent: ClickParam) => {
                            this.handleClick(clickEvent);
                        }}
                        style={{ height: "100%" }}
                    >
                        <Menu.Item key="userinfo">用户信息</Menu.Item>
                        <Menu.Item key="editinfo">编辑信息</Menu.Item>
                        <Menu.Item key="createteam">创建队伍</Menu.Item>
                        <Menu.SubMenu key="myteams" title="我的队伍">
                            <Menu.Item key="created">已创建</Menu.Item>
                            <Menu.Item key="requested">已申请</Menu.Item>
                            <Menu.Item key="joined">已加入</Menu.Item>
                        </Menu.SubMenu>
                        <Menu.SubMenu key="mymsgs" title="我的消息">
                            <Menu.Item key="joinmsgs">申请加入</Menu.Item>
                            <Menu.Item key="quitmsgs">申请退出</Menu.Item>
                            <Menu.Item key="infomsgs">通知消息</Menu.Item>
                        </Menu.SubMenu>
                        <Menu.Item key="logout">注销</Menu.Item>
                    </Menu>
                </Sider>
                <Layout>
                    <Row>
                        <Col offset={4} span={16}>{content}</Col>
                    </Row>
                </Layout>
            </Layout>
        );
    }
}

export default UserCenter;
