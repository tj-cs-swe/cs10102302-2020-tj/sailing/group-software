package com.tongji.domain;

import java.util.Date;

public class Logs {//通知信息类
    private String id;
    private String uid;//关联用户id
    private String msg;//信息内容
    private Date ltime;//通知时间
    private int status;//通知状态 0未读 1以读

    public Date getLtime() {
        return ltime;
    }

    public void setLtime(Date ltime) {
        this.ltime = ltime;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
