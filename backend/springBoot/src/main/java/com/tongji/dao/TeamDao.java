package com.tongji.dao;

import com.tongji.domain.Team;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

@Repository
public interface TeamDao {

    /**
     * 根据队伍名去数据库查找team
     * @param teamname 队伍名
     * @return team封装类
     */
    @Select("select*from team where teamname=#{teamname}")
    @Results({
            @Result(column = "id",property = "id",id = true),
            @Result(column = "uid",property ="uid" ),
            @Result(column ="teamname" ,property ="teamname" ),
            @Result(column ="description" ,property ="description" ),
            @Result(column = "requirements",property = "requirements"),
            @Result(column ="deadline" ,property ="deadline" ),
            @Result(column = "number",property = "number"),
            @Result(column = "sta",property = "status"),
            @Result(column ="id" ,property = "labels",many =@Many(select ="com.tongji.dao.LabelDao.findByTeamId" ))
    })
    public Team findByTeamName(String teamname);


    /**
     * 根据队伍id查找队伍详情
     * @param teamId 队伍id
     * @return 查找到的队伍
     */
    @Select("select*from team where id=#{teamId}")
    @Results({
            @Result(column = "id",property = "id",id = true),
            @Result(column = "uid",property ="uid" ),
            @Result(column ="teamname" ,property ="teamname" ),
            @Result(column ="description" ,property ="description" ),
            @Result(column = "requirements",property = "requirements"),
            @Result(column ="deadline" ,property ="deadline" ),
            @Result(column = "number",property = "number"),
            @Result(column = "sta",property = "status"),
            @Result(column ="id" ,property = "labels",many =@Many(select ="com.tongji.dao.LabelDao.findByTeamId" ))
    })
    public Team findByTeamId(String teamId);


    /**
     * 查询所有队伍
     * @return list
     */
    @Select("select*from team ")
    @Results({
            @Result(column = "id",property = "id",id = true),
            @Result(column = "uid",property ="uid" ),
            @Result(column ="teamname" ,property ="teamname" ),
            @Result(column ="description" ,property ="description" ),
            @Result(column = "requirements",property = "requirements"),
            @Result(column ="deadline" ,property ="deadline" ),
            @Result(column = "number",property = "number"),
            @Result(column = "sta",property = "status"),
            @Result(column ="id" ,property = "labels",many =@Many(select ="com.tongji.dao.LabelDao.findByTeamId" ))
    })
    public List<Team> findAll();



    /**
     * 向数据库中添加一个队伍
     * @param team
     */
    @Insert("insert into team values(#{team.id},#{team.uid},#{team.teamname},#{team.description},#{team.requirements},#{team.deadline},#{team.number},#{team.status})")
    public void addTeam(@Param("team") Team team);



    /**
     * 添加标签
     * @param team_id 队伍id
     * @param label_id 标签id
     */
    @Insert("insert into team_label values(#{team_id},#{label_id})")
    public void addLabel(@Param("team_id") String team_id,@Param("label_id") String label_id);


    /**
     * 删除该队伍的所有标签
     * @param team_id 队伍id
     */
    @Delete("delete from team_label where team_id=#{team_id}")
    public void deleteAllLabel(String team_id);



    /**
     * 根据队伍ID进行队伍信息的更新
     * @param team
     */
    @Update("update team set teamname=#{team.teamname},description=#{team.description},requirements=#{team.requirements},deadline=#{team.deadline} where id=#{team.id}")
    public void updateById(@Param("team") Team team);


    /**
     * 根据关键词查找队伍
     * @param word 关键词
     * @return 队伍列表
     */
    @Select("select*from team where teamname like CONCAT('%',#{word},'%') or description like CONCAT('%',#{word},'%') or requirements like CONCAT('%',#{word},'%')")
    @Results({
            @Result(column = "id",property = "id",id = true),
            @Result(column = "uid",property ="uid" ),
            @Result(column ="teamname" ,property ="teamname" ),
            @Result(column ="description" ,property ="description" ),
            @Result(column = "requirements",property = "requirements"),
            @Result(column ="deadline" ,property ="deadline" ),
            @Result(column = "number",property = "number"),
            @Result(column = "sta",property = "status"),
            @Result(column ="id" ,property = "labels",many =@Many(select ="com.tongji.dao.LabelDao.findByTeamId" ))
    })
    public List<Team> findByWord(String word);


    /**
     * 查找具有某个标签的队伍id
     * @param label_id 标签id
     * @return 队伍id集合
     */
    @Select("select team_id from team_label where label_id=#{label_id}")
    public List<String> findByLabel(@Param("label_id") String label_id);


    /**
     * 根据id集合,查找队伍
     * @param teamids 队伍id集合
     * @return 符合条件的队伍
     */
    @Select("<script>"+"select*from team where id in"+"<foreach item='item' index='index' collection='ids' open='(' separator=',' close=')'>"
    +"#{item}"+"</foreach>"+"</script>")
    @Results({
            @Result(column = "id",property = "id",id = true),
            @Result(column = "uid",property ="uid" ),
            @Result(column ="teamname" ,property ="teamname" ),
            @Result(column ="description" ,property ="description" ),
            @Result(column = "requirements",property = "requirements"),
            @Result(column ="deadline" ,property ="deadline" ),
            @Result(column = "number",property = "number"),
            @Result(column = "sta",property = "status"),
            @Result(column ="id" ,property = "labels",many =@Many(select ="com.tongji.dao.LabelDao.findByTeamId" ))
    })
    public List<Team>findByIds(@Param("ids") Set<String>teamids);


    //-------------------------------------------------------------------------------------------

    /**
     * 查找用户申请的队伍的id
     * @param uid 用户id
     * @return 队伍id集合
     */
    @Select("select team_id from user_tb_team where user_id=#{uid}")
    public List<String>findApplyTeamId(@Param("uid") String uid);


    /**
     * 查找用户加入队伍的id
     * @param uid 用户id
     * @return 队伍id集合
     */
    @Select("select team_id from user_team where user_id=#{uid}")
    public List<String>findJoinTeamId(@Param("uid") String uid);


    /**
     * 查找用户创建的队伍
     * @param uid 用户id
     * @return 队伍集合
     */
    @Select("select*from team where uid=#{uid}")
    @Results({
            @Result(column = "id",property = "id",id = true),
            @Result(column = "uid",property ="uid" ),
            @Result(column ="teamname" ,property ="teamname" ),
            @Result(column ="description" ,property ="description" ),
            @Result(column = "requirements",property = "requirements"),
            @Result(column ="deadline" ,property ="deadline" ),
            @Result(column = "number",property = "number"),
            @Result(column = "sta",property = "status"),
            @Result(column ="id" ,property = "labels",many =@Many(select ="com.tongji.dao.LabelDao.findByTeamId" ))
    })
    public List<Team>findCreateTeam(@Param("uid") String uid);


    //添加删除，申请人和加入人

    @Insert("insert into  user_tb_team values(#{uid},#{teamid})")
    public void addApplyUser(@Param("uid") String uid,@Param("teamid") String teamid);

    @Insert("insert into  user_team values(#{uid},#{teamid})")
    public void addJoinUser(@Param("uid")String uid,@Param("teamid") String teamid);

    @Insert("insert into  user_drop_team values(#{uid},#{teamid})")
    public void adddropUser(@Param("uid") String uid,@Param("teamid") String teamid);


    @Delete("delete from user_tb_team where user_id=#{uid} and team_id=#{teamid}")
    public void deleteApplyUser(@Param("uid")String uid,@Param("teamid") String teamid);

    @Delete("delete from user_team where user_id=#{uid} and team_id=#{teamid}")
    public void deleteJoinUser(@Param("uid")String uid,@Param("teamid") String teamid);

    @Delete("delete from user_drop_team where user_id=#{uid} and team_id=#{teamid}")
    public void deleteDropUser(@Param("uid")String uid,@Param("teamid") String teamid);


    //更改队伍人员信息
    @Update("update team set number=#{number} where id=#{id}")
    public void updateMember(@Param("id") String teamid,@Param("number") int number);


    //删除队伍
    @Delete("delete from team where id=#{id}")
    public void deleteTeam(@Param("id") String teamid);

    //删除队伍成员
    @Delete("delete from user_team where team_id=#{id}")
    public void deleteAllMember(@Param("id") String teamid);

    //删除所有申请加入
    @Delete("delete from user_tb_team where team_id=#{id}")
    public void deleteAllJoinApply(@Param("id") String teamid);

    //删除所有申请退出
    @Delete("delete from user_drop_team where team_id=#{id}")
    public void deleteAllDropApply(@Param("id") String teamid);


    //完成组队
    @Update("update team set sta=1 where id=#{id}")
    public void complete(@Param("id") String teamid);


}
