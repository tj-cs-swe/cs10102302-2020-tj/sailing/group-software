package com.tongji.controller;

import com.tongji.domain.*;
import com.tongji.service.UserService;
import com.tongji.util.Utils;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.ibatis.annotations.Many;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@Controller
@RequestMapping("/user")
public class UserController {

    @Autowired
    UserService userService;


    //登录处理
    @RequestMapping("/login")
    @ResponseBody
    public LoginInfo login(@RequestParam(value = "username") String username, @RequestParam("password") String password,HttpServletResponse response,HttpServletRequest request){


        User byUsername = userService.findByUsername(username);
        LoginInfo info=new LoginInfo();

        if(byUsername==null){
            info.setStatus(false);
            info.setMsg("用户名不存在");
        }else{
            if(byUsername.getPassword().equals(password)){
                info.setStatus(true);
                info.setMsg("登录成功");

                Cookie cookie=new Cookie("userId",byUsername.getId());//创建一个cookie,存储登录用户的id
                cookie.setPath("/");
                cookie.setMaxAge(60*60*12);//最大生命周期为半天
                HttpSession session = request.getSession();
                session.setAttribute(byUsername.getId(),byUsername);//将user对象存入session
                response.addCookie(cookie);//返回cookie

            }else{
                info.setStatus(false);
                info.setMsg("密码错误");
            }
        }


        //System.out.println(info);
        return info;


    }


    //注册处理
    @RequestMapping("/register")
    @ResponseBody
    public RegisterInfo register(@RequestBody Map<String,Object> parm) throws InvocationTargetException, IllegalAccessException {

        User user= new User();
        BeanUtils.populate(user,parm);
        user.setId(UUID.randomUUID().toString());
        RegisterInfo registerInfo = userService.register(user);
        return registerInfo;
    }



    //注销处理
    @RequestMapping("/loginOut")
    public void loginOut(HttpServletRequest request,HttpServletResponse response){
        Cookie[] cookies = request.getCookies();
        String value="";

        //删除userId cookie
        for (Cookie cookie : cookies) {

            if(cookie.getName().equals("userId")){
                value=cookie.getValue();
                cookie.setMaxAge(0);
                cookie.setPath("/");
                response.addCookie(cookie);
            }
        }

        //删除session
        HttpSession session = request.getSession();

        session.removeAttribute(value);

    }


    //查看个人信息
    @RequestMapping("/myInfo")
    @ResponseBody
    public User findByUseId(HttpServletRequest request){
        //System.out.println("myInfo");

        Cookie[] cookies = request.getCookies();
        String value=Utils.getUid(cookies);
        User user = userService.findById(value);


        return user;
    }


    //更新个人信息
    @RequestMapping("/updateInfo")
    @ResponseBody
    public String updateUser(@RequestBody Map<String,Object> parm,HttpServletRequest request) throws InvocationTargetException, IllegalAccessException {

        User user=new User();
        BeanUtils.populate(user,parm);
        user.setId(Utils.getUid(request.getCookies()));
        //System.out.println("updateInfo");

        userService.updateInfo(user);


        return "个人信息修改成功";
    }


    //*****************************************************************************************


    //查找队员信息
    @RequestMapping("/findTeamMember")
    @ResponseBody
    public List<User>findTeamMember(@RequestBody Map<String,Object> parm){
        List<User> mem=userService.findJoinUser((String) parm.get("teamid"));
        mem.add(userService.findCreateUser((String) parm.get("teamid")));
        return  mem;

    }

    //查找申请退出的队员信息
    @RequestMapping("/findDropMember")
    @ResponseBody
    public List<User>findDropMember(@RequestBody Map<String,Object> parm){
        return userService.findDropUser((String) parm.get("teamid"));
    }


    //查找申请加入的队员信息
    @RequestMapping("/findApplyMember")
    @ResponseBody
    public List<User>findApplyMember(@RequestBody Map<String,Object> parm){
        return userService.findApplyUser((String) parm.get("teamid"));
    }


    //查找队伍创建人的信息
    @RequestMapping("/findCreateMember")
    @ResponseBody
    public User findCreateMember(@RequestBody Map<String,Object> parm){
        return userService.findCreateUser((String) parm.get("teamid"));
    }
}
