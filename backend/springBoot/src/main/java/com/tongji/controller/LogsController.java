package com.tongji.controller;

import com.tongji.domain.Logs;
import com.tongji.service.LogsService;
import com.tongji.util.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/logs")
public class LogsController {

    @Autowired
    LogsService logsService;

    //查找未读信息
    @RequestMapping("/findUnread")
    public List<Logs>findUnread(HttpServletRequest request){
        return logsService.findUnread(Utils.getUid(request.getCookies()));

    }

    //查找以读信息
    @RequestMapping("/findread")
    public List<Logs>findread(HttpServletRequest request){
        return  logsService.findread(Utils.getUid(request.getCookies()));
    }

    //确认信息
    @RequestMapping("/confirm")
    public void confirm(@RequestBody Map<String,Object>map){
        logsService.confirmLog((String) map.get("id"));
    }
}
