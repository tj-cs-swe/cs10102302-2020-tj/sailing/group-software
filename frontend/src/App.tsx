import * as React from "react";
import { Layout, Menu, Row, Input, Col, Breadcrumb, message, Button } from "antd";
import { ClickParam } from "antd/es/menu";

import * as Cookie from "./components/Cookie";
import LoginForm from "./components/LoginForm";
import RegistrationForm from "./components/RegisterationForm";
import UserCenter from "./components/UserCenter";
import MainPage, { MainPageProps } from "./components/MainPage";
import "./App.css";

const { Header, Content } = Layout;

interface AppState {
    login: boolean;
    uid: string;
    current: string;
    mainprops: MainPageProps;
}

class App extends React.Component<{}, AppState> {
    constructor(props: {}) {
        super(props);
        let uid = Cookie.getCookie("uid");
        if (uid === "") {
            this.state = {
                login: false,
                uid: "",
                current: "1",
                mainprops: {}
            };
        } else {
            this.state = {
                login: true,
                uid: uid,
                current: "1",
                mainprops: {}
            };
        }
    }

    handleClick(e: ClickParam) {
        this.setState({
            current: e.key !== "3" ? e.key : this.state.current,
        });
    }

    handleLogin(uid: string, failed: boolean) {
        if (!failed) {
            Cookie.setCookie("uid", uid, 1);
            this.setState({
                login: true,
                uid: uid,
                current: "1",
            });
        }
        else {
            Cookie.clearCookie("uid");
            this.setState({
                login: false,
                uid: "",
                current: "1",
            });
        }
    }

    handleRegisteration() {
        this.setState({
            login: false,
            current: "1",
        });
    }

    handleLogout() {
        Cookie.clearCookie("uid");
        this.setState({
            login: false,
            uid: "",
            current: "1",
        });
    }

    handleSearch(keyword: string) {
        const team_request = new Request("team/findByWord", {
            method: "POST",
            headers: {
                "Content-Type": "application/json; charset=utf-8",
            },
            body: JSON.stringify({
                word: keyword,
            }),
            cache: "reload",
        });
        fetch(team_request)
            .then(
                (response: Response) => {
                    return response.json();
                },
                (reason: any) => {
                    message.error("获取队伍列表失败：网络异常！");
                    console.log(reason);
                }
            )
            .then(
                (response: any) => {
                    console.log("获取队伍列表成功" + response);
                    this.setState({
                        current: "1",
                        mainprops: {
                            searchTeams: response,
                        }
                    });
                    this.forceUpdate();
                },
                (reason: any) => {
                    message.error("获取队伍列表失败：服务器异常！");
                    console.log(reason);
                }
            );
    }

    backHome() {
        this.setState({
            current: "1",
            mainprops: {}
        });
        this.forceUpdate();
    }

    render() {
        if (!this.state.login) {
            const content =
                this.state.current === "1" ? (
                    <LoginForm
                        onLogin={(username: string, failed: boolean) => {
                            this.handleLogin(username, failed);
                        }}
                    />
                ) : (
                        <RegistrationForm
                            onRegisteration={() => {
                                this.handleRegisteration();
                            }}
                        />
                    );
            return (
                <Layout>
                    <Header className="header">
                        <div className="logo" />
                        <Menu
                            theme="dark"
                            mode="horizontal"
                            defaultSelectedKeys={["1"]}
                            onClick={(e: ClickParam) => {
                                this.handleClick(e);
                            }}
                        >
                            <Menu.Item key="1">登录</Menu.Item>
                            <Menu.Item key="2">注册</Menu.Item>
                        </Menu>
                    </Header>
                    <Content>
                        <Row
                            align="middle"
                            justify="center"
                            style={{ height: "100vh" }}
                        >
                            {content}
                        </Row>
                    </Content>
                </Layout>
            );
        } else {
            let content: React.ReactNode;
            if (this.state.current === "1") {
                content = (
                    <MainPage
                        teamid={this.state.mainprops.teamid}
                        searchTeams={this.state.mainprops.searchTeams}
                    />
                )
            } else {
                content = (
                    <UserCenter
                        onLogout={() => {
                            this.handleLogout();
                        }}
                        backHome={() => {
                            this.backHome();
                        }}
                        toTeam={(id: string) => {
                            this.setState({
                                current: "1",
                                mainprops: {
                                    teamid: id,
                                }
                            })
                        }}
                    />
                );
            }
            return (
                <Layout>
                    <Header className="header">
                        <div className="logo" />
                        <Menu
                            theme="dark"
                            mode="horizontal"
                            defaultSelectedKeys={["1"]}
                            onClick={(e: ClickParam) => {
                                this.handleClick(e);
                            }}
                        >
                            <Menu.Item key="1">主页</Menu.Item>
                            <Menu.Item key="3">
                                <Input.Search
                                    placeholder="请输入搜索内容"
                                    enterButton="搜索"
                                    size="large"
                                    onSearch={(value) => {
                                        this.handleSearch(value);
                                    }}
                                />
                            </Menu.Item>
                            <Menu.Item key="2" style={{ float: "right" }}>个人中心</Menu.Item>
                        </Menu>
                    </Header>
                    <Content>{content}</Content>
                </Layout>
            );
        }
    }
}

export default App;
