import * as React from "react";
import {
    Typography,
    Divider,
    Button,
    DatePicker,
    TimePicker,
    Select,
    message,
    Card,
} from "antd";
import moment from "moment";
import { LabelInfo, applyTeam, findCreate, findApply, findJoin, User, findTeamMember } from "./WebApi";

const { Title, Paragraph, Text } = Typography;
const { Option } = Select;

interface TeamInfoProps {
    id: string;
}

interface TeamInfoStates {
    show: boolean;
    editable: boolean;
    changed: boolean;
    status: boolean;
    teamname: string;
    members: User[];
    description: string;
    requirements: string;
    date: string;
    time: string;
    number: number;
    labels: string[];
    all_tags: LabelInfo[];
}

class TeamInfo extends React.Component<TeamInfoProps, TeamInfoStates> {
    constructor(props: TeamInfoProps) {
        super(props);

        this.state = {
            show: false,
            editable: false,
            changed: false,
            status: false,
            teamname: "null",
            members: [],
            description: "null",
            requirements: "null",
            date: moment().format("YYYY-MM-DD"),
            time: moment().format("HH:mm:SS"),
            number: 0,
            labels: [],
            all_tags: [],
        };

        this.update()

        findTeamMember(this.props.id).then((users) => {
            this.setState({
                members: users,
            });
        }, (reason) => {
            message.error("获取队伍成员失败");
        })

        const tag_request = new Request("label/findAll", {
            cache: "reload",
        });
        fetch(tag_request)
            .then(
                (response: Response) => {
                    return response.json();
                },
                (reason: any) => {
                    message.error("获取标签失败：网络异常！");
                    console.log(reason);
                }
            )
            .then(
                (response: any) => {
                    console.log("获取标签成功" + response);
                    this.setState({
                        all_tags: response,
                    });
                },
                (reason: any) => {
                    message.error("获取标签失败：服务器异常！");
                    console.log(reason);
                }
            );

        const team_request = new Request("team/findDetail", {
            method: "POST",
            headers: {
                "Content-Type": "application/json; charset=utf-8",
            },
            body: JSON.stringify({
                teamId: this.props.id,
            }),
            cache: "reload",
        });
        fetch(team_request)
            .then(
                (response: Response) => {
                    return response.json();
                },
                (reason: any) => {
                    message.error("获取队伍信息失败：网络异常！");
                    console.log(reason);
                }
            )
            .then(
                (response: any) => {
                    console.log("获取队伍信息成功" + response);
                    const datetime = moment(response.deadline);
                    this.setState({
                        status: response.status,
                        teamname: response.teamname,
                        description: response.description,
                        requirements: response.requirements,
                        date: datetime.format("YYYY-MM-DD"),
                        time: datetime.format("HH:mm:ss"),
                        number: response.number,
                        labels: response.labels.map(
                            (value: any, index: any, array: any) => {
                                return value.id;
                            }
                        ),
                    });
                },
                (reason: any) => {
                    message.error("获取队伍信息失败：服务器异常！");
                    console.log(reason);
                }
            );
    }

    update() {
        (async () => {
            let result: boolean = true;
            let editable: boolean = false;
            let teams = await findCreate();
            for (const team of teams) {
                if (team.id === this.props.id) {
                    editable = true;
                    result = false;
                    break;
                }
            }
            teams = await findApply();
            for (const team of teams) {
                if (team.id === this.props.id) {
                    result = false;
                    break;
                }
            }
            teams = await findJoin();
            for (const team of teams) {
                if (team.id === this.props.id) {
                    result = false;
                    break;
                }
            }
            this.setState({
                show: result,
                editable: editable,
            });
        })();
    }

    handleClick() {
        const create_request = new Request(`team/update`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json; charset=utf-8",
            },
            body: JSON.stringify({
                team: {
                    id: this.props.id,
                    teamname: this.state.teamname,
                    description: this.state.description,
                    requirements: this.state.requirements,
                    deadline: this.state.date + " " + this.state.time,
                },
                label_ids: this.state.labels,
            }),
            cache: "reload",
        });
        fetch(create_request)
            .then(
                (response: Response) => {
                    return response.text();
                },
                (reason: any) => {
                    message.error("修改失败：网络异常！");
                    console.log(reason);
                }
            )
            .then(
                (response: any) => {
                    console.log(response);
                    message.success(response);
                    this.setState({
                        changed: false,
                    });
                },
                (reason: any) => {
                    message.error("修改失败：服务器异常！");
                    console.log(reason);
                }
            );
    }

    handleTitleChange(value: string) {
        console.log(value);
        this.setState({
            changed: true,
            teamname: value,
        });
    }

    handleDescriptionChange(value: string) {
        console.log(value);
        this.setState({
            changed: true,
            description: value,
        });
    }

    handleRequirementsChange(value: string) {
        console.log(value);
        this.setState({
            changed: true,
            requirements: value,
        });
    }

    handleDateChange(date: any, dateString: string) {
        console.log(date, dateString);
        let new_date = date.format("YYYY-MM-DD");
        this.setState({
            changed: true,
            date: new_date,
        });
    }

    handleTimeChange(time: any, timeString: string) {
        console.log(time, timeString);
        let new_time = time.format("HH:mm:ss");
        this.setState({
            changed: true,
            time: new_time,
        });
    }

    handleTagsChange(value: any) {
        console.log(value);
        this.setState({
            changed: true,
            labels: value,
        });
    }

    handleRequest() {
        applyTeam(this.props.id).then((msg) => {
            message.info(msg);
            this.setState({ show: false });
        })
        this.setState({ show: false });
    }

    render(): React.ReactNode {
        const tags = this.state.all_tags.map((value, index, array) => {
            return <Option value={value.id}>{value.labelname}</Option>;
        });
        const button = this.state.changed ? (
            <>
                <Divider />
                <Button
                    type="primary"
                    onClick={(e) => {
                        this.handleClick();
                    }}
                >
                    修改信息
                </Button>
            </>
        ) : (
                <></>
            );
        return (
            <Typography>
                <Title
                    level={2}
                    editable={this.state.editable ? {
                        onChange: (value: string) => {
                            this.handleTitleChange(value);
                        },
                    } : false}
                >
                    {this.state.teamname}
                </Title>
                {this.state.show && !this.state.status ?
                    <span><Button
                        type="primary"
                        onClick={() => { this.handleRequest() }}>
                        申请加入
                    </Button></span> :
                    <span></span>}
                <Divider />
                <Paragraph>
                    <Text
                        editable={this.state.editable ? {
                            onChange: (value: string) => {
                                this.handleDescriptionChange(value);
                            },
                        } : false}
                    >
                        {this.state.description}
                    </Text>
                </Paragraph>
                <Divider />
                <Paragraph>
                    <Title level={4}>成员列表</Title>
                    {this.state.members.map((user) => {
                        return <Card>{user.nickname}</Card>
                    })}
                </Paragraph>
                <Divider />
                <Paragraph>
                    <Title level={4}>人员要求</Title>
                    <Text
                        editable={this.state.editable ? {
                            onChange: (value: string) => {
                                this.handleRequirementsChange(value);
                            },
                        } : false}
                    >
                        {this.state.requirements}
                    </Text>
                </Paragraph>
                <Divider />
                <Paragraph>
                    <Title level={4}>截止日期</Title>
                    <DatePicker
                        onChange={this.state.editable ? (date, dateString) => {
                            this.handleDateChange(date, dateString);
                        } : undefined}
                        defaultValue={moment(this.state.date, "YYYY-MM-DD")}
                    />
                    <TimePicker
                        onChange={this.state.editable ? (time, timeString) => {
                            this.handleTimeChange(time, timeString);
                        } : undefined}
                        defaultValue={moment(this.state.time, "HH:mm:SS")}
                    />
                </Paragraph>
                <Divider />
                <Paragraph>
                    <Title level={4}>队伍人数</Title>
                    {this.state.number}
                </Paragraph>
                <Divider />
                <Paragraph>
                    <Title level={4}>队伍标签</Title>
                    <Select
                        mode="tags"
                        placeholder="Please select"
                        onChange={this.state.editable ? (value) => {
                            this.handleTagsChange(value);
                        } : undefined}
                        value={this.state.labels}
                        style={{ width: "100%" }}
                    >
                        {tags}
                    </Select>
                </Paragraph>
                {button}
            </Typography>
        );
    }
}

export default TeamInfo;
