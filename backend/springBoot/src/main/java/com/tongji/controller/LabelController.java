package com.tongji.controller;

import com.tongji.domain.Label;
import com.tongji.service.LabelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/label")
public class LabelController {

    @Autowired
    LabelService labelService;

    //查询所有标签
    @RequestMapping("/findAll")
    public List<Label>findAll(){
        return labelService.findAll();
    }

    //查找不属于队伍的标签
    @RequestMapping("/findOther")
    public List<Label>findOther(@RequestParam("teamId") String teamId){
        return labelService.findOther(teamId);
    }

}
