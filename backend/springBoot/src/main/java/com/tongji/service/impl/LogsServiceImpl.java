package com.tongji.service.impl;

import com.tongji.dao.LogsDao;
import com.tongji.domain.Logs;
import com.tongji.service.LogsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LogsServiceImpl implements LogsService {
    @Autowired
    LogsDao logsDao;

    @Override
    public List<Logs> findUnread(String uid) {
        return logsDao.findUnread(uid,0);
    }

    @Override
    public List<Logs> findread(String uid) {
        return logsDao.findread(uid,1);
    }

    @Override
    public void confirmLog(String id) {
        logsDao.confirmLog(id,1);
    }
}
