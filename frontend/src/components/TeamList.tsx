import React from "react";
import { List } from "antd";
import { FullTeamInfo } from "./WebApi";

interface TeamListProps {
    data: FullTeamInfo[];
    toTeam(id: string): void;
}

interface TeamListStates {}

class TeamList extends React.Component<TeamListProps, TeamListStates> {
    constructor(props: TeamListProps) {
        super(props);
    }

    render(): React.ReactNode {
        console.log("Team List Render");
        return (
            <List
                itemLayout="vertical"
                size="large"
                pagination={{
                    onChange: (page) => {
                        console.log(page);
                    },
                    pageSize: 20,
                }}
                dataSource={this.props.data}
                renderItem={(item) => (
                    <List.Item
                        key={item.teamname}
                    >
                        <List.Item.Meta
                            title={<a onClick={()=>{this.props.toTeam(item.id)}}>队伍名称 {item.teamname}</a>}
                            description={"队伍描述 " + item.description}
                        />
                        人员要求 {item.requirements}
                    </List.Item>
                )}
            />
        );
    }
}

export default TeamList;
