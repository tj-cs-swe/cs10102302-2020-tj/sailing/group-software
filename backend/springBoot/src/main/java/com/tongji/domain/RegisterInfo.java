package com.tongji.domain;

//注册信息反馈
public class RegisterInfo {
    private boolean status;//注册状态 false为登录失败 true为登录成功
    private String msg;//注册信息记录

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
