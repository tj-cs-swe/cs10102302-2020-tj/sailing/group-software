package com.tongji.service;

import com.tongji.domain.ReInfo;
import com.tongji.domain.Team;

import java.util.List;

public interface TeamService {

    public ReInfo createTeam(Team team, List<String>ids);//创建队伍


    public void updateTeam(Team team,List<String>ids);//更新队伍信息


    public Team findById(String teamId);//查找队伍详情


    public List<Team> findAll();//查找所有的队伍


    public List<Team> findByWord(String word);//根据关键词查找队伍


    public List<Team> findByLabel(List<String>label_ids);//根据标签筛选队伍

    //--------------------------------------------------------------------------

    public List<Team>findApplyTeam(String uid);//查找申请队伍

    public List<Team>findJoinTeam(String uid);//查找加入队伍

    public List<Team>findCreateTeam(String uid);//查找创建的队伍


    public void applyTeam(String uid,String teamid);//用户申请加入队伍

    public void rejectApply(String uid,String teamid);//队长拒绝加入

    public void joinTeam(String uid,String teamid);//同意加入队伍

    public void quitApply(String uid,String teamid);//用户取消申请


    public void applyDropTeam(String uid,String teamid);//用户申请退出队伍

    public void rejectDrop(String uid,String teamid);//队长拒绝退出

    public void dropTeam(String uid,String teamid);//同意退出队伍

    public void quitDrop(String uid,String teamid);//用户取消退出


    public ReInfo completeTeam(String teamid);//完成组队


    public void breakTeam(String teamid);//解散队伍


}
