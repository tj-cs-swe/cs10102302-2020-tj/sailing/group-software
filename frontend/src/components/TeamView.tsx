import React from "react";
import { Card } from "antd";
import { FullTeamInfo } from "./WebApi";

interface TeamViewProps {
    data: FullTeamInfo[];
    type: "created" | "requested" | "joined";
    toTeam(id: string): void;
    onFinish(id: string): void;
    onCancel(id: string): void;
}

interface TeamViewStates {
}

class TeamView extends React.Component<TeamViewProps, TeamViewStates> {
    constructor(props: TeamViewProps) {
        super(props);
    }

    render(): React.ReactNode {
        console.log("Team List Render");
        const totitle = (t: FullTeamInfo) => { return <a onClick={() => { this.props.toTeam(t.id) }}>队伍名称 {t.teamname}</a>; }
        if (this.props.type === "created") {
            return this.props.data.map((t) => {
                return (
                    <Card
                        title={totitle(t)}
                        actions={t.status ? [
                            <a onClick={() => { this.props.onCancel(t.id) }}>删除</a>
                        ] :
                            [
                                <a onClick={() => { this.props.onFinish(t.id) }}>完成</a>,
                                <a onClick={() => { this.props.onCancel(t.id) }}>删除</a>
                            ]}>
                        {t.description}
                    </Card>
                );
            });
        } else if (this.props.type === "requested") {
            return this.props.data.map((t) => {
                return (
                    <Card
                        title={totitle(t)}
                        actions={[<a onClick={() => { this.props.onCancel(t.id) }}>取消申请</a>]}>
                        {t.description}
                    </Card>
                );
            });
        } else {
            return this.props.data.map((t) => {
                return (
                    <Card
                        title={totitle(t)}
                        actions={[<a onClick={() => { this.props.onCancel(t.id) }}>退出队伍</a>]}>
                        {t.description}
                    </Card>
                );
            });
        }
    }
}

export default TeamView;
