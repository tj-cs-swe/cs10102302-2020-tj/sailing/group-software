import React from "react";
import { Card } from "antd";
import { Logs } from "./WebApi";

interface InfoViewProps {
    data: Logs[]
    onFinish(id: string): void;
}

interface InfoViewStates {
}

class InfoView extends React.Component<InfoViewProps, InfoViewStates> {
    constructor(props: InfoViewProps) {
        super(props);
    }

    render(): React.ReactNode {
        console.log("Msg List Render");
        return this.props.data.map((t) => {
            return (
                <Card
                    title="通知信息"
                    actions={[
                        <a onClick={() => { this.props.onFinish(t.id) }}>已读</a>]}>
                    {t.msg}
                </Card>
            );
        });
    }
}

export default InfoView;
